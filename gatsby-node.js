const axios = require('axios');
const crypto = require('crypto');

exports.createPages = async ({ actions }) => {
  const { createPage } = actions
  createPage({
    path: "/using-dsg",
    component: require.resolve("./src/templates/using-dsg.js"),
    context: {},
    defer: true,
  })
}

exports.sourceNodes = async ({ actions }) => {
  const { createNode } = actions;

  const fetchGoogleDriveJson = () => axios.get(`https://opensheet.elk.sh/1Z9OM8cqUlRObxr1O-O-o9NVOhvjiIVO1qCsu6NT7FWY/1`);
  const res = await fetchGoogleDriveJson();

  res.data.map((entry, i) => {
    const userNode = {
      id: `${i}`,
      parent: `__SOURCE__`,
      internal: {
        type: `GoogleDriveJson`,
      },
      children: [],
      entry: entry
    }

    const contentDigest = crypto
      .createHash(`md5`)
      .update(JSON.stringify(userNode))
      .digest(`hex`);
    userNode.internal.contentDigest = contentDigest;

    createNode(userNode);
  });

  return;
}