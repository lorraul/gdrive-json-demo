import * as React from "react"
import { Link, graphql } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import Layout from "../components/layout"
import Seo from "../components/seo"

const IndexPage = ({ data }) => {
  return (
  <Layout>
    <Seo title="Home" />
    <h1>Hi people</h1>
    <p>
      {data.allGoogleDriveJson.edges.map(({ node }) => (
        <p key={node.id}>
          {JSON.stringify(node)}
        </p>
      ))}
    </p>
  </Layout>
)}

export default IndexPage

export const query = graphql`
query {
  allGoogleDriveJson {
    edges {
      node {
        id
        entry {
          attendance
          city
          date
        }
      }
    }
  }
}
`